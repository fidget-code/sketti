project (1.0.0 ➜ 1.0.0)
├── inquirer (^12.3.2 ➜ 12.3.2)
│   ├── @inquirer/core (^10.1.4 ➜ 10.1.4)
│   │   ├── @inquirer/figures (^1.0.9 ➜ 1.0.9)
│   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   ├── ansi-escapes (^4.3.2 ➜ 4.3.2) ⭯ LOOP
│   │   ├── cli-width (^4.1.0 ➜ 4.1.0)
│   │   ├── mute-stream (^2.0.0 ➜ 2.0.0) ⭯ LOOP
│   │   ├── signal-exit (^4.1.0 ➜ 4.1.0)
│   │   ├── strip-ansi (^6.0.1 ➜ 6.0.1)
│   │   │   └── ansi-regex (^5.0.1 ➜ 5.0.1)
│   │   ├── wrap-ansi (^6.2.0 ➜ 6.2.0)
│   │   │   ├── ansi-styles (^4.0.0 ➜ 4.3.0)
│   │   │   │   └── color-convert (^2.0.1 ➜ 2.0.1)
│   │   │   │       └── color-name (~1.1.4 ➜ 1.1.4)
│   │   │   ├── string-width (^4.1.0 ➜ 4.2.3)
│   │   │   │   ├── emoji-regex (^8.0.0 ➜ 8.0.0)
│   │   │   │   ├── is-fullwidth-code-point (^3.0.0 ➜ 3.0.0)
│   │   │   │   └── strip-ansi (^6.0.1 ➜ 6.0.1)
│   │   │   │       └── ansi-regex (^5.0.1 ➜ 5.0.1)
│   │   │   └── strip-ansi (^6.0.0 ➜ 6.0.1)
│   │   │       └── ansi-regex (^5.0.1 ➜ 5.0.1) ⭯ LOOP
│   │   └── yoctocolors-cjs (^2.1.2 ➜ 2.1.2)
│   ├── @inquirer/prompts (^7.2.3 ➜ 7.2.3)
│   │   ├── @inquirer/checkbox (^4.0.6 ➜ 4.0.6)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4)
│   │   │   │   ├── @inquirer/figures (^1.0.9 ➜ 1.0.9) ⭯ LOOP
│   │   │   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   │   │   ├── ansi-escapes (^4.3.2 ➜ 4.3.2) ⭯ LOOP
│   │   │   │   ├── cli-width (^4.1.0 ➜ 4.1.0)
│   │   │   │   ├── mute-stream (^2.0.0 ➜ 2.0.0)
│   │   │   │   ├── signal-exit (^4.1.0 ➜ 4.1.0)
│   │   │   │   ├── strip-ansi (^6.0.1 ➜ 6.0.1)
│   │   │   │   │   └── ansi-regex (^5.0.1 ➜ 5.0.1)
│   │   │   │   ├── wrap-ansi (^6.2.0 ➜ 6.2.0)
│   │   │   │   │   ├── ansi-styles (^4.0.0 ➜ 4.3.0)
│   │   │   │   │   │   └── color-convert (^2.0.1 ➜ 2.0.1)
│   │   │   │   │   │       └── color-name (~1.1.4 ➜ 1.1.4)
│   │   │   │   │   ├── string-width (^4.1.0 ➜ 4.2.3)
│   │   │   │   │   │   ├── emoji-regex (^8.0.0 ➜ 8.0.0)
│   │   │   │   │   │   ├── is-fullwidth-code-point (^3.0.0 ➜ 3.0.0)
│   │   │   │   │   │   └── strip-ansi (^6.0.1 ➜ 6.0.1)
│   │   │   │   │   │       └── ansi-regex (^5.0.1 ➜ 5.0.1)
│   │   │   │   │   └── strip-ansi (^6.0.0 ➜ 6.0.1)
│   │   │   │   │       └── ansi-regex (^5.0.1 ➜ 5.0.1) ⭯ LOOP
│   │   │   │   └── yoctocolors-cjs (^2.1.2 ➜ 2.1.2) ⭯ LOOP
│   │   │   ├── @inquirer/figures (^1.0.9 ➜ 1.0.9)
│   │   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2)
│   │   │   ├── ansi-escapes (^4.3.2 ➜ 4.3.2)
│   │   │   │   └── type-fest (^0.21.3 ➜ 0.21.3)
│   │   │   └── yoctocolors-cjs (^2.1.2 ➜ 2.1.2)
│   │   ├── @inquirer/confirm (^5.1.3 ➜ 5.1.3)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │   │   └── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   ├── @inquirer/editor (^4.2.3 ➜ 4.2.3)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   │   └── external-editor (^3.1.0 ➜ 3.1.0)
│   │   │       ├── chardet (^0.7.0 ➜ 0.7.0)
│   │   │       ├── iconv-lite (^0.4.24 ➜ 0.4.24)
│   │   │       │   └── safer-buffer (>= 2.1.2 < 3 ➜ 2.1.2)
│   │   │       └── tmp (^0.0.33 ➜ 0.0.33)
│   │   │           └── os-tmpdir (~1.0.2 ➜ 1.0.2)
│   │   ├── @inquirer/expand (^4.0.6 ➜ 4.0.6)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   │   └── yoctocolors-cjs (^2.1.2 ➜ 2.1.2) ⭯ LOOP
│   │   ├── @inquirer/input (^4.1.3 ➜ 4.1.3)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │   │   └── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   ├── @inquirer/number (^3.0.6 ➜ 3.0.6)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │   │   └── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   ├── @inquirer/password (^4.0.6 ➜ 4.0.6)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   │   └── ansi-escapes (^4.3.2 ➜ 4.3.2) ⭯ LOOP
│   │   ├── @inquirer/rawlist (^4.0.6 ➜ 4.0.6)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   │   └── yoctocolors-cjs (^2.1.2 ➜ 2.1.2) ⭯ LOOP
│   │   ├── @inquirer/search (^3.0.6 ➜ 3.0.6)
│   │   │   ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │   │   ├── @inquirer/figures (^1.0.9 ➜ 1.0.9) ⭯ LOOP
│   │   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   │   └── yoctocolors-cjs (^2.1.2 ➜ 2.1.2) ⭯ LOOP
│   │   └── @inquirer/select (^4.0.6 ➜ 4.0.6)
│   │       ├── @inquirer/core (^10.1.4 ➜ 10.1.4) ⭯ LOOP
│   │       ├── @inquirer/figures (^1.0.9 ➜ 1.0.9) ⭯ LOOP
│   │       ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │       ├── ansi-escapes (^4.3.2 ➜ 4.3.2) ⭯ LOOP
│   │       └── yoctocolors-cjs (^2.1.2 ➜ 2.1.2) ⭯ LOOP
│   ├── @inquirer/type (^3.0.2 ➜ 3.0.2)
│   ├── ansi-escapes (^4.3.2 ➜ 4.3.2)
│   │   └── type-fest (^0.21.3 ➜ 0.21.3) ⭯ LOOP
│   ├── mute-stream (^2.0.0 ➜ 2.0.0)
│   ├── run-async (^3.0.0 ➜ 3.0.0)
│   └── rxjs (^7.8.1 ➜ 7.8.1)
│       └── tslib (^2.1.0 ➜ 2.8.1)
├── lodash (^4.17.21 ➜ 4.17.21)
├── chalk (^5.4.1 ➜ 5.4.1)
├── esbuild (^0.24.2 ➜ 0.24.2)
└── typescript (^5.7.3 ➜ 5.7.3)

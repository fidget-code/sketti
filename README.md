# Sketti

> A NodeJS Dependency Mapper - Making sense of the spaghetti that is node dependencies

![Dynamic TOML Badge](https://img.shields.io/badge/dynamic/toml?url=https%3A%2F%2Fgitlab.com%2Ffidget-code%2Fsketti%2F-%2Fraw%2Fmain%2FCargo.toml%3Fref_type%3Dheads&query=%24.package.version&style=for-the-badge&label=version)
![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/fidget-code%2Fsketti?branch=main&style=for-the-badge)
![GitLab License](https://img.shields.io/gitlab/license/fidget-code%2Fsketti?style=for-the-badge&color=blue)
![Static Badge](https://img.shields.io/badge/status-active-blue?style=for-the-badge)

![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/fidget-code%2Fsketti?style=for-the-badge)
![GitLab Issues](https://img.shields.io/gitlab/issues/open/fidget-code%2Fsketti?style=for-the-badge)
![GitLab Issues](https://img.shields.io/gitlab/issues/closed/fidget-code%2Fsketti?style=for-the-badge)
![GitLab Merge Requests](https://img.shields.io/gitlab/merge-requests/open/fidget-code%2Fsketti?style=for-the-badge)
![GitLab Merge Requests](https://img.shields.io/gitlab/merge-requests/merged/fidget-code%2Fsketti?style=for-the-badge)

![GitLab Release](https://img.shields.io/gitlab/v/release/fidget-code%2Fsketti?sort=semver&display_name=release&style=for-the-badge&label=Gitlab%20Release)
![NPM Version](https://img.shields.io/npm/v/sketti?style=for-the-badge)

The goal of this project is to help visualize your dependency tree. 

Finding dependency loops and where dependencies originated from can be hard. 

This aims to make that a little easier.

## Features

- [X] Npm package
- [X] Run against local repo and generate report
- [X] Yarn detection and basic support
- [X] Filtering of dependencies via `Regex`
- [X] Multiple report formats and styles
- [X] Written in Rust to improve performance
- [X] Only requires NPM to be installed if in Yarn mode
- [X] Progress and Timing indication
- [X] Fast.
  - Projects with 10,000+ dependency links finish in reasonable time.
  - Filtering applied first to reduce expensive ops

## Future

- [ ] Add tests
- [ ] Multiplatform support
- [ ] Improved performance
  - [ ] Support parallel ops
  - [ ] Report rendering speeds
  - [X] Tree traversal/creation improvements
- [ ] Additional report formats
- [ ] Additional control over report style
- [X] Code deduplication
- [ ] Better Yarn support
- [ ] Support newer linking options for `node_modules`

## How to

### Install 

#### Option 1 - `Gitlab Releases`

1. Navigate to the [Releases](https://gitlab.com/fidget-code/sketti/-/releases)
2. Download the latest or desired version.
3. Place in or add location to `PATH` for easier calling

#### Option 2 - `NPM`

1. Run `npm install -g sketti`

#### Option 3 - `Compile Source`

1. Fork and Clone repository
2. Ensure you have requirements
   - Visual Studio 2017-2022 w/ `Desktop Development with C++`
   - Rust install with `--profile default`
3. Ensure your toolchain is `x86_64-pc-windows-msvc`
4. Run `cargo build --release`

### Usage

> Assumptions are made the tool is in your `PATH` if not fully qualify the tool's path.

Open a terminal and navigate to the root of the repository you wish to run against. This is the directory with the 
`package.json` file.

The tool expects a successful install to have completed. If the repository is fresh or in an odd state do an install.

The call signature is `sketti [OPTIONS]` as you can see all values are optional and blow defines them and their defaults

| Option                              | Values                                                                                    | Default         | Notes                                                          |
|-------------------------------------|-------------------------------------------------------------------------------------------|-----------------|----------------------------------------------------------------|
| `--directory, --dir, --folder`      | `String<Path>`                                                                            | `.\`            | the path can be relative or absolute                           |
| `--debug, -d`                       | `None`                                                                                    | `None`          | this flag can be used multiple times                           |
| `--yarn-mode, --yarn`               | `None`                                                                                    | `None`          |                                                                |
| `--tree`                            | `None`                                                                                    | `None`          |                                                                |
| `--release`                         | `None`                                                                                    | `None`          |                                                                |
| `--report-type, --report, --export` | `none, html, html-tree, txt-tree, mermaid, txt-list, svg, png, jpg, md, xml, static-html` | `none`          | string value comma separated for multiple                      |
| `--report-name, --file-name`        | `String`                                                                                  | `sketti-report` | this is the base name. report type and extension will be added |
| `--filter`                          | `String<Regex>`                                                                           | `None`          | used against package names                                     |
| `--depth`                           | `int`                                                                                     | `MAX`           |                                                                |
| `--help, -h`                        | `None`                                                                                    | `None`          |                                                                |
| `--version, -V`                     | `None`                                                                                    | `None`          |                                                                |

#### Option `--directory`

> Used to specify the directory to operate on

**Aliases**:
- `--dir`
- `--folder`

#### Option `--debug`

> Used to increase the messaging for additional information or debugging

**Aliases**:
- `-d`

**Notes**:
- Can be used multiple times
- After 3 uses the level is at max
- You cannot use it like `-d 3 or -d=3`
- Messaging can and will slow results especially at max. Use with caution.

#### Option `--yarn-mode`

> Used to specify the project uses yarn instead of npm. yarn 1.x supported

**Aliases**:
- `--yarn`

**Notes**:
- Will create a `package-lock.json` temporarily for tool use
- This mode is not as accurate as native npm because of difference in resolution methods
- The tool requires npm be globally available to perform conversion of current `node_modules` into a `pacakge-lock.json` file

#### Option `--tree`

> Used to output the text based tree to the console when complete

#### Option `--release`

> Used to only include production dependencies in the project

#### Option `--report-type`

> Used to specify the reports generated post-processing

**Aliases**:
- `--report`
- `--export`

**Notes**:
- You can specify one or many 
- To use multiple separate with commas like `--report static-html,html,txt-tree`
- More information on reports can be found below

#### Option `--report-name`

> Used to set the base name for the report files

**Aliases**:
- `--file-name`

#### Option `--filter`

> Used to filter the packages processed to those you want

**Notes**:
- Is a regex expression as a string
- Use anchors if needed 
- Example to only look at one scope `^@some-scope/.*`
- The tree's branch and detection ends when a package has no dependencies that match the regex

#### Option `--depth`

> Used to control the recursion depth 

#### Option `--help`

> Show help

**Aliases**:
- `-h`

**Notes**:
- `-h` will show standard help
- `--help` will show the long help

#### Option `--version`

> Show version

**Aliases**:
- `-V`

### Example Output

![sketti output](docs/media/output-logs.png "Sketti Output")

![sketti output](docs/media/output-loop.gif)

### Reports

Examples of reports can be found [here](docs/output-examples)

#### Image based reports

Image based reports are all based on the mermaid chart. They are just exposed in multiple formats for ease of user use.

SVG output is the translation point for all other image types. If you have a weird output with an image output try the 
svg output and see if it also shows it. 

**Example**

![sketti jpg](docs/media/sketti-report-image.jpg "Sketti Report Jpg")

#### Txt based reports

The tree variant matches the output of the `--tree` cli option. 

**Example**
```
project (1.0.0 ➜ 1.0.0)
├── inquirer (^12.3.2 ➜ 12.3.2)
│   ├── @inquirer/core (^10.1.4 ➜ 10.1.4)
│   │   ├── @inquirer/figures (^1.0.9 ➜ 1.0.9)
│   │   ├── @inquirer/type (^3.0.2 ➜ 3.0.2) ⭯ LOOP
│   │   ├── ansi-escapes (^4.3.2 ➜ 4.3.2) ⭯ LOOP
│   │   ├── cli-width (^4.1.0 ➜ 4.1.0)
│   │   ├── mute-stream (^2.0.0 ➜ 2.0.0) ⭯ LOOP
│   │   ├── signal-exit (^4.1.0 ➜ 4.1.0)
│   │   ├── strip-ansi (^6.0.1 ➜ 6.0.1)
│   │   │   └── ansi-regex (^5.0.1 ➜ 5.0.1)
│   │   ├── wrap-ansi (^6.2.0 ➜ 6.2.0)
│   │   │   ├── ansi-styles (^4.0.0 ➜ 4.3.0)
│   │   │   │   └── color-convert (^2.0.1 ➜ 2.0.1)
│   │   │   │       └── color-name (~1.1.4 ➜ 1.1.4)
│   │   │   ├── string-width (^4.1.0 ➜ 4.2.3)
│   │   │   │   ├── emoji-regex (^8.0.0 ➜ 8.0.0)
│   │   │   │   ├── is-fullwidth-code-point (^3.0.0 ➜ 3.0.0)
│   │   │   │   └── strip-ansi (^6.0.1 ➜ 6.0.1)
│   │   │   │       └── ansi-regex (^5.0.1 ➜ 5.0.1)
│   │   │   └── strip-ansi (^6.0.0 ➜ 6.0.1)
│   │   │       └── ansi-regex (^5.0.1 ➜ 5.0.1) ⭯ LOOP
│   │   └── yoctocolors-cjs (^2.1.2 ➜ 2.1.2)
│   ├── @inquirer/prompts (^7.2.3 ➜ 7.2.3)
...
```

The list option is a flat list of all unique packages and versions.

**Example**
```
@inquirer/checkbox (^4.0.6 ➜ 4.0.6)
@inquirer/confirm (^5.1.3 ➜ 5.1.3)
@inquirer/core (^10.1.4 ➜ 10.1.4)
@inquirer/editor (^4.2.3 ➜ 4.2.3)
@inquirer/expand (^4.0.6 ➜ 4.0.6)
@inquirer/figures (^1.0.9 ➜ 1.0.9)
@inquirer/input (^4.1.3 ➜ 4.1.3)
@inquirer/number (^3.0.6 ➜ 3.0.6)
@inquirer/password (^4.0.6 ➜ 4.0.6)
@inquirer/prompts (^7.2.3 ➜ 7.2.3)
@inquirer/rawlist (^4.0.6 ➜ 4.0.6)
@inquirer/search (^3.0.6 ➜ 3.0.6)
@inquirer/select (^4.0.6 ➜ 4.0.6)
@inquirer/type (^3.0.2 ➜ 3.0.2)
ansi-escapes (^4.3.2 ➜ 4.3.2)
ansi-regex (^5.0.1 ➜ 5.0.1)
ansi-styles (^4.0.0 ➜ 4.3.0)
...
```

#### Xml report

The xml report is a custom easy to parse report following a basic tree structure.

**Example**
```xml
<dependencies>
    <dependency-group>
        <dependency requested="^12.3.2" resolved="12.3.2" name="inquirer" loop="false"/>
        <dependency-group>
            <dependency requested="^10.1.4" resolved="10.1.4" name="@inquirer/core" loop="false"/>
            <dependency-group>
                <dependency requested="^1.0.9" resolved="1.0.9" name="@inquirer/figures" loop="false"/>
                <dependency requested="^3.0.2" resolved="3.0.2" name="@inquirer/type" loop="true"/>
                <dependency requested="^4.3.2" resolved="4.3.2" name="ansi-escapes" loop="true"/>
                <dependency requested="^4.1.0" resolved="4.1.0" name="cli-width" loop="false"/>
                <dependency requested="^2.0.0" resolved="2.0.0" name="mute-stream" loop="true"/>
                <dependency requested="^4.1.0" resolved="4.1.0" name="signal-exit" loop="false"/>
                <dependency requested="^6.0.1" resolved="6.0.1" name="strip-ansi" loop="false"/>
                <dependency-group>
                    <dependency requested="^5.0.1" resolved="5.0.1" name="ansi-regex" loop="false"/>
                </dependency-group>
...
```

#### Mermaid report

This is an output of the mermaid code used to render other reports. If you wish to modify the mermaid code then generate
your own mermaid report this allows that

**Example**
```
flowchart TB
NODE1@{ label: "**project**\n1.0.0 ➜ 1.0.0", shape: card }
class NODE1 primary
subgraph SUBGRAPH2 [`inquirer`]
  direction LR
  NODE2@{ label: "**inquirer**\n^12.3.2 ➜ 12.3.2", shape: odd }
  class NODE2 secondary
  subgraph SUBGRAPH7 [`@inquirer/core`]
    direction LR
    NODE7@{ label: "**@inquirer/core**\n^10.1.4 ➜ 10.1.4", shape: odd }
    class NODE7 secondary
    NODE14@{ label: "**@inquirer/figures**\n^1.0.9 ➜ 1.0.9", shape: hexagon }
    class NODE14 tertiary
...
```

#### Md report

This takes the mermaid report and embeds it in a markdown file. Nothing more or less. Requires a Markdown viewer that 
supports mermaid

#### Html reports

`html-tree` is the only html report to not use mermaid. The tree is the same as the text and cli output.

The `html` and `static-html` are the same in terms of output. The difference is behaviour in the browser. 

##### Html 

This output trades performance in the browser for performance in the tool.
This places everything needed into the webpage but the graph is built and constructed in the browser on page load.

This is fine for small graphs but larger graphs can take time. There is a loading animation displayed while it is loading.

##### Static-Html

This output trades performance in the tool for performance in the browser.
This takes the normal html output and loads it in a headless browser to construct the graph and then saves the results.

This allows for near instant loads in the browser but greatly lengthens the report generation time in large graphs.
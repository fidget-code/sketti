#!/usr/bin/env bash

if [ $# -eq 0 ]; then
  CHANGELOG=$(<CHANGELOG.md)
  while [[ $CHANGELOG =~ \%\%([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})\%\% ]]; do
    match="${BASH_REMATCH[0]}"
    email="${BASH_REMATCH[1]}"
    id=$(glab api /users?search=$email | grep -o '"username":"[^"]*' | grep -o '[^"]*$')
    if [[ -n "$id" ]]; then
      id="@$id"
    elif [[ "$email" == 'kmason@extron.com' ]]; then
      id='@mediatech15'
    fi
    echo "$match | $email | $id" >&2
    CHANGELOG="${CHANGELOG//${match}/${id}}"
  done
  echo "$CHANGELOG" >&2
  echo "$CHANGELOG" > CHANGELOG.md
else
  CHANGELOG=$1
  while [[ $CHANGELOG =~ \%\%([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})\%\% ]]; do
    match="${BASH_REMATCH[0]}"
    email="${BASH_REMATCH[1]}"
    id=$(glab api /users?search=$email | grep -o '"username":"[^"]*' | grep -o '[^"]*$')
    if [[ -n "$id" ]]; then
      id="@$id"
    else [[ "$email" == 'kmason@extron.com' ]]
      id='@mediatech15'
    fi
    echo "$match | $email | $id" >&2
    CHANGELOG="${CHANGELOG//${match}/${id}}"
  done
  echo "$CHANGELOG"
fi
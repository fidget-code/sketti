use crate::report::ExportType;
use clap::ArgAction;
use clap::ColorChoice;
use clap::Parser;
use clap::ValueHint;
use indicatif::*;
use lazer::lazer;
use regex::Regex;
use static_init::dynamic;
use std::path::Path;
use std::path::PathBuf;
use std::sync::OnceLock;
use std::time::Duration;

#[derive(Parser)]
#[command(term_width = 0)]
#[command(version = env!("CARGO_PKG_VERSION"))]
#[command(name = "Sketti")]
#[command(author = "Fidget Code")]
#[command(about = "A NodeJS Dependency Mapper")]
#[command(flatten_help = true)]
#[command(color = ColorChoice::Auto)]
#[command(help_template = "\
{name} - {about}
{tab}Making sense of the spaghetti that is node dependencies
{tab}Version: {version}
{tab}Author: {author}

{before-help}{usage-heading}
{tab}{usage}

{all-args}{after-help}
")]
pub struct CliOptions {
  #[arg(long)]
  #[arg(visible_aliases = ["dir", "folder"])]
  #[arg(value_name = "FILE")]
  #[arg(default_value = "./")]
  #[arg(value_hint = ValueHint::DirPath)]
  #[arg(help = "folder containing nodejs project")]
  #[arg(long_help = "folder containing nodejs project")]
  pub directory: Option<PathBuf>,

  #[arg(short)]
  #[arg(long)]
  #[arg(action = ArgAction::Count)]
  #[arg(help = "enables additional messaging")]
  #[arg(long_help = "enables additional messaging\nmaybe used multiple times")]
  pub debug: u8,

  #[arg(long)]
  #[arg(help = "enables yarn support")]
  #[arg(visible_aliases = ["yarn"])]
  #[arg(long_help = "enables yarn support\nwill generate a package-lock.json temporarily")]
  pub yarn_mode: bool,

  #[arg(long)]
  #[arg(help = "shows the tree at the end")]
  #[arg(long_help = "shows the tree at the end")]
  pub tree: bool,

  #[arg(long)]
  #[arg(value_enum)]
  #[arg(value_delimiter = ',')]
  #[arg(value_name = "TYPE")]
  #[arg(default_value = "none")]
  #[arg(visible_aliases = ["report", "export"])]
  #[arg(help = "report generation format")]
  #[arg(long_help = "report generation format")]
  pub report_type: Vec<ExportType>,

  #[arg(long)]
  #[arg(value_name = "NAME")]
  #[arg(default_value = "sketti-report")]
  #[arg(visible_aliases = ["file-name"])]
  #[arg(help = "report file basename")]
  #[arg(long_help = "report file basename\nno extension")]
  pub report_name: String,

  #[arg(long)]
  #[arg(value_name = "FILTER")]
  #[arg(value_parser = is_valid_regex)]
  #[arg(help = "regex dependency filter for project")]
  #[arg(
    long_help = "regex dependency filter for project\nrestricts processing to continue as long as the filter matches"
  )]
  pub filter: Option<Regex>,

  #[arg(long)]
  #[arg(help = "only process release dependencies")]
  #[arg(long_help = "only process release dependencies in the base project")]
  pub release: bool,

  #[arg(long)]
  #[arg(default_value_t = usize::MAX)]
  #[arg(help = "sets max child depth")]
  #[arg(long_help = "sets max child depth\ndefault is max depth")]
  pub depth: usize,
}

fn is_valid_regex(s: &str) -> Result<Regex, String> {
  match Regex::new(s) {
    Ok(r) => Ok(r),
    Err(e) => Err(format!("invalid regex: {}", e)),
  }
}

pub struct Output {
  cli_pre_size: usize,
  err_pre: String,
  warn_pre: String,
  info_pre: String,
  debug_pre: String,
  verbose_pre: String,
  trace_pre: String,
}

impl Output {
  fn new() -> Self {
    let pre_size = 10;
    let mut ip = "[INFO".to_string();
    ip.push_str(" ".repeat(pre_size - 5).as_str());
    ip.push_str("] ");
    let mut wp = "[WARN".to_string();
    wp.push_str(" ".repeat(pre_size - 5).as_str());
    wp.push_str("] ");
    let mut ep = "[ERROR".to_string();
    ep.push_str(" ".repeat(pre_size - 6).as_str());
    ep.push_str("] ");
    let mut dp = "[DEBUG".to_string();
    dp.push_str(" ".repeat(pre_size - 6).as_str());
    dp.push_str("] ");
    let mut vp = "[VERBOSE".to_string();
    vp.push_str(" ".repeat(pre_size - 8).as_str());
    vp.push_str("] ");
    let mut tp = "[TRACE".to_string();
    tp.push_str(" ".repeat(pre_size - 6).as_str());
    tp.push_str("] ");
    Output {
      cli_pre_size: pre_size,
      warn_pre: wp,
      err_pre: ep,
      info_pre: ip,
      debug_pre: dp,
      verbose_pre: vp,
      trace_pre: tp,
    }
  }

  pub fn write_error(&self, message: &str, exit: bool) {
    lazer().print_red(&self.err_pre).print_ln(message);
    if exit {
      std::process::exit(1)
    }
  }

  pub fn write_info(&self, message: &str) {
    lazer().print_cyan(&self.info_pre).print_ln(message);
  }

  pub fn write_warn(&self, message: &str) {
    lazer().print_yellow(&self.warn_pre).print_ln(message);
  }

  pub fn write_debug(&self, message: &str) {
    if *DebugMode.read() > 0 {
      lazer().print_magenta(&self.debug_pre).print_ln(message);
    }
  }

  pub fn write_verbose(&self, message: &str) {
    if *DebugMode.read() > 1 {
      lazer().print_magenta(&self.verbose_pre).print_ln(message);
    }
  }

  pub fn write_trace(&self, message: &str) {
    if *DebugMode.read() > 2 {
      lazer().print_magenta(&self.trace_pre).print_ln(message);
    }
  }

  pub fn write_args(&self, opts: &Cli) {
    if *DebugMode.read() > 0 {
      let d_pre = &self.debug_pre;
      let rep_types: Vec<String> = opts.report_type.iter().map(|r| r.to_string()).collect();
      let filter = opts.filter.clone().unwrap_or(Regex::new("None").unwrap()).as_str().to_string();
      lazer()
        .print_magenta(d_pre)
        .print_ln("Options in use:")
        .print_magenta(d_pre)
        .print("  Debug Level: ")
        .print_yellow_ln(DebugMode.read().to_string().as_str())
        .print_magenta(d_pre)
        .print("  Yarn: ")
        .iff(opts.yarn_mode)
        .print_yellow_ln(opts.yarn_mode.to_string().as_str())
        .eliff(!opts.yarn_mode)
        .print_green_ln(opts.yarn_mode.to_string().as_str())
        .end()
        .print_magenta(d_pre)
        .print("  Directory: ")
        .print_blue_ln(opts.abs_directory.display().to_string().as_str())
        .print_magenta(d_pre)
        .print("  Show Tree: ")
        .print_yellow_ln(opts.show_tree.to_string().as_str())
        .print_magenta(d_pre)
        .print("  Report Name: ")
        .print_blue_ln(opts.report_name.to_string().as_str())
        .print_magenta(d_pre)
        .print("  Report Types: ")
        .print_yellow_ln(rep_types.join(", ").as_str())
        .print_magenta(d_pre)
        .print("  Filter: ")
        .print_yellow_ln(filter.as_str())
        .print_magenta(d_pre)
        .print("  Depth: ")
        .print_yellow_ln(opts.depth.to_string().as_str())
        .print_magenta(d_pre)
        .print("  Release Only: ")
        .print_yellow_ln(opts.release_only.to_string().as_str())
        .print_magenta_ln(d_pre);
    }
  }

  pub fn colorize(&self, message: &str, color: &str, item: &str) -> String {
    let mut p = lazer();
    let buf = p.buffer().print(message).print_space(1);
    if color == "yellow" {
      buf.print_yellow(item);
    } else if color == "red" {
      buf.print_red(item);
    } else if color == "green" {
      buf.print_green(item);
    } else if color == "blue" {
      buf.print_blue(item);
    } else if color == "magenta" {
      buf.print_magenta(item);
    } else if color == "cyan" {
      buf.print_cyan(item);
    } else {
      buf.print(item);
    }

    buf.ret()
  }

  pub fn show_progress(&self, message: String) -> ProgressBar {
    let mut bar = ProgressBar::new(100);
    bar.set_draw_target(ProgressDrawTarget::stderr_with_hz(60));
    bar.set_prefix(lazer().buffer().print_cyan(&self.info_pre).ret());
    bar = bar.with_style(ProgressStyle::with_template("{prefix}{spinner} {elapsed} {msg}").unwrap().tick_strings(&[
      "▐⠂       ▌",
      "▐⠈       ▌",
      "▐ ⠂      ▌",
      "▐ ⠠      ▌",
      "▐  ⡀     ▌",
      "▐  ⠠     ▌",
      "▐   ⠂    ▌",
      "▐   ⠈    ▌",
      "▐    ⠂   ▌",
      "▐    ⠠   ▌",
      "▐     ⡀  ▌",
      "▐     ⠠  ▌",
      "▐      ⠂ ▌",
      "▐      ⠈ ▌",
      "▐       ⠂▌",
      "▐       ⠠▌",
      "▐       ⡀▌",
      "▐      ⠠ ▌",
      "▐      ⠂ ▌",
      "▐     ⠈  ▌",
      "▐     ⠂  ▌",
      "▐    ⠠   ▌",
      "▐    ⡀   ▌",
      "▐   ⠠    ▌",
      "▐   ⠂    ▌",
      "▐  ⠈     ▌",
      "▐  ⠂     ▌",
      "▐ ⠠      ▌",
      "▐ ⡀      ▌",
      "▐⠠       ▌",
    ]));
    let tick = Duration::from_millis(50);
    bar.enable_steady_tick(tick);
    bar.set_message(message);
    bar
  }

  pub fn end_progress(&self, bar: ProgressBar, clear: bool) {
    let msg = bar.message();
    bar.finish_and_clear();
    if !clear {
      Out.write_info(&msg);
    }
  }

  pub fn update_progress(&self, bar: ProgressBar, message: String) {
    bar.set_message(message);
  }
}

pub struct Cli {
  options: CliOptions,
  pub debug: u8,
  pub yarn_mode: bool,
  pub directory: PathBuf,
  pub package_json: PathBuf,
  pub package_lock: PathBuf,
  pub yarn_lock: PathBuf,
  pub abs_directory: PathBuf,
  pub report_type: Vec<ExportType>,
  pub report_name: String,
  pub show_tree: bool,
  pub filter: Option<Regex>,
  pub release_only: bool,
  pub depth: usize,
}

#[dynamic(lazy)]
pub static Out: Output = Output::new();

#[dynamic(lazy)]
pub static mut DebugMode: u8 = 0;

#[dynamic(lazy)]
pub static Filter: OnceLock<Regex> = OnceLock::new();

#[dynamic(lazy)]
pub static DepthMax: OnceLock<usize> = OnceLock::new();

impl Cli {
  pub fn new() -> Self {
    let opts = CliOptions::parse();
    let dir = opts.directory.as_deref().unwrap().to_owned();
    let d = opts.debug.to_owned();
    let ym = opts.yarn_mode.to_owned();
    let r = opts.report_type.to_owned();
    let rn = opts.report_name.to_owned();
    let st = opts.tree.to_owned();
    let fil = opts.filter.to_owned();
    let ro = opts.release.to_owned();
    let dep = opts.depth.to_owned();
    *DebugMode.write() = d;
    if fil.is_some() {
      Filter.set(fil.clone().unwrap()).unwrap();
    }
    DepthMax.set(dep).unwrap();

    let abs_dir_res = dunce::canonicalize(Path::new(&dir).to_owned());
    if abs_dir_res.is_err() {
      Out.write_warn(Out.colorize("Does not exist", "blue", Path::new(&dir).display().to_string().as_str()).as_str());
      Out.write_error("The path supplied was not valid", true);
    }
    let abs_dir = abs_dir_res.unwrap();
    let pkg_json = abs_dir.join("package.json");
    let pkg_lock = abs_dir.join("package-lock.json");
    let yarn = abs_dir.join("yarn.lock");

    Cli {
      options: opts,
      debug: d,
      yarn_mode: ym,
      directory: dir,
      package_json: pkg_json,
      package_lock: pkg_lock,
      yarn_lock: yarn,
      abs_directory: abs_dir,
      report_name: rn,
      report_type: r,
      show_tree: st,
      filter: fil,
      release_only: ro,
      depth: dep,
    }
  }

  pub fn validate(&self) {
    Out.write_args(&self);

    Out.write_debug(&Out.colorize("Normalized Path:", "blue", &self.abs_directory.display().to_string()));
    Out.write_debug(&Out.colorize("package.json Path:", "blue", &self.package_json.display().to_string()));
    Out.write_debug(&Out.colorize("package.json Exists:", "yellow", &self.package_json.exists().to_string()));
    Out.write_debug(&Out.colorize("package-lock.json Path:", "blue", &self.package_lock.display().to_string()));
    Out.write_debug(&Out.colorize("package-lock.json Exists:", "yellow", &self.package_lock.exists().to_string()));
    Out.write_debug(&Out.colorize("yarn.lock Path:", "blue", &self.yarn_lock.display().to_string()));
    Out.write_debug(&Out.colorize("yarn.lock Exists:", "yellow", &self.yarn_lock.exists().to_string()));
    Out.write_debug("");

    let mut had_error = false;
    if !self.package_json.exists() {
      had_error = true;
      Out.write_error("package.json does not exist in directory", false);
    }
    if !self.package_lock.exists() && !self.yarn_mode {
      had_error = true;
      Out.write_error("package-lock.json does not exist in directory", false);
    }
    if !self.yarn_lock.exists() && self.yarn_mode {
      had_error = true;
      Out.write_error("yarn.lock does not exist in directory", false);
    }
    if had_error {
      Out.write_error("Errors during validation. Exiting", true);
    }
  }
}

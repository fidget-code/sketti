use crate::dependency::{Dependency, DependencyTree, TreeNodeId};
use crate::file_io::{write_generic_file, write_generic_file_bytes};
use crate::utils::Timer;
use clap::ValueEnum;
use dom_query::Document;
use ego_tree::iter::Edge;
use ego_tree::NodeRef;
use headless_chrome::{browser, Browser, LaunchOptionsBuilder, Tab};
use image::codecs::jpeg::JpegEncoder;
use image::DynamicImage;
use indicatif::ProgressBar;
use quick_xml::escape::escape;
use resvg::tiny_skia::Color;
use resvg::usvg::{FontResolver, ImageRendering, ShapeRendering, TextRendering};
use resvg::{tiny_skia, usvg};
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;

#[derive(PartialEq, Copy, Clone, ValueEnum)]
pub enum ExportType {
  None,
  Html,
  HtmlTree,
  TxtTree,
  Mermaid,
  TxtList,
  Svg,
  Png,
  Jpg,
  Md,
  Xml,
  StaticHtml,
}

impl std::fmt::Display for ExportType {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    match self {
      ExportType::None => write!(f, "None"),
      ExportType::Html => write!(f, "Html"),
      ExportType::HtmlTree => write!(f, "Html Tree"),
      ExportType::TxtTree => write!(f, "Text Tree"),
      ExportType::Mermaid => write!(f, "Mermaid"),
      ExportType::TxtList => write!(f, "Text List"),
      ExportType::Svg => write!(f, "Svg"),
      ExportType::Png => write!(f, "Png"),
      ExportType::Jpg => write!(f, "Jpeg"),
      ExportType::Md => write!(f, "Markdown"),
      ExportType::Xml => write!(f, "Xml"),
      ExportType::StaticHtml => write!(f, "Static Html"),
    }
  }
}

struct Mermaid {
  mermaid_html: String,
  index_html: String,
  mermaid_logic_html: String,
  mermaid_css: String,
  browser: Browser,
  tab: Arc<Tab>,
  project_name: String,
}

impl Mermaid {
  fn new(project_name: String) -> Self {
    let installed_chrome;
    match browser::default_executable() {
      Ok(p) => {
        installed_chrome = Some(p);
      },
      Err(_) => {
        installed_chrome = None;
      },
    }
    let b = Browser::new(
      LaunchOptionsBuilder::default()
        .idle_browser_timeout(Duration::from_secs(600))
        .headless(true)
        .devtools(false)
        .enable_logging(false)
        .path(installed_chrome)
        .build()
        .unwrap(),
    )
    .unwrap();
    let t = b.new_tab().unwrap();
    Mermaid {
      mermaid_html: include_str!("../content/mermaid.html").to_string(),
      mermaid_logic_html: include_str!("../content/mermaid-logic.html").to_string(),
      index_html: include_str!("../content/index.html").to_string(),
      mermaid_css: include_str!("../content/mermaid.css").to_string(),
      browser: b,
      tab: t,
      project_name,
    }
  }

  fn get_html_doc(&self) -> Document {
    let index_html = self.index_html.clone();
    Document::from(index_html)
  }

  fn get_html_string(&self, doc: Document) -> String {
    doc.html().to_string()
  }

  fn replace_mermaid_html(&self, doc: Document) -> Document {
    let script = self.mermaid_html.clone();
    let script_logic = self.mermaid_logic_html.clone();
    let script_css = self.mermaid_css.clone();
    let script_tag = doc.select_single("#mermaid-source");
    script_tag.replace_with_html(script);
    let logic_tag = doc.select_single("#mermaid-logic");
    logic_tag.replace_with_html(script_logic);
    let css_tag = doc.select_single("#mermaid-css");
    css_tag.set_text(script_css.as_str());
    doc
  }

  fn replace_mermaid_content(&self, doc: Document, content: String) -> Document {
    let content_tag = doc.select_single("#mermaid-data");
    content_tag.set_html(content);
    doc
  }

  fn generate_html(&self, content: String) -> String {
    let mut doc = self.get_html_doc();
    doc = self.replace_mermaid_html(doc);
    doc = self.replace_mermaid_content(doc, content);

    let page_source = self.get_html_string(doc);
    page_source
  }

  fn generate_mermaid_syntax(&self, tree: DependencyTree) -> String {
    let mut syntax = String::from("flowchart TB\n");
    let mut iter = tree.get_root().traverse().peekable();
    let mut id_path: Vec<String> = Vec::new();
    let mut sub_graph_path: Vec<String> = Vec::new();
    let mut links: Vec<String> = Vec::new();
    while iter.peek().is_some() {
      let item = iter.next().unwrap();
      let mut make_node = false;
      let mut add_path = false;
      let mut remove_path = false;
      let mut node_has_child = false;
      let mut node: Option<NodeRef<Dependency>> = None;
      match item {
        Edge::Open(n) if n.has_children() => {
          // next in current level next will be below
          // add to id_path && create node
          make_node = true;
          add_path = true;
          node_has_child = true;
          node = Some(n);
        },
        Edge::Open(n) if n.next_sibling().is_some() => {
          // next in current level no children
          // create node
          make_node = true;
          node = Some(n);
        },
        Edge::Open(n) => {
          // end of siblings current level
          // create node
          make_node = true;
          node = Some(n);
        },
        Edge::Close(n) if n.has_children() => {
          // moved up in levels more items at this level (maybe added)
          // remove from id_path && don't make node
          remove_path = true;
          node = Some(n);
        },
        _ => {},
      }
      match node {
        Some(n) => {
          let node_id = n.id();
          let tree_node = TreeNodeId::new(node_id);
          let mut node_name = String::from("NODE");
          node_name.push_str(tree_node.to_string().as_str());
          let mut indent = " ".repeat(sub_graph_path.len() * 2);
          let mut sub_name = String::from("SUBGRAPH");
          let mut made_sub_node = false;
          if make_node {
            if node_has_child && !id_path.is_empty() {
              // container node
              sub_name.push_str(tree_node.to_string().as_str());
              syntax.push_str(indent.as_str());
              syntax.push_str("subgraph ");
              syntax.push_str(sub_name.as_str());
              syntax.push_str(" [`");
              syntax.push_str(n.value().name.as_str());
              syntax.push_str("`]\n");
              sub_graph_path.push(sub_name.clone());
              indent = " ".repeat(sub_graph_path.len() * 2);
              syntax.push_str(indent.as_str());
              syntax.push_str("direction LR\n");
              made_sub_node = true;
            }
            syntax.push_str(indent.as_str());
            syntax.push_str(node_name.as_str());
            syntax.push_str("@{ label: \"**");
            syntax.push_str(n.value().name.as_str());
            syntax.push_str("** (");
            syntax.push_str(n.value().kind.to_short_string().as_str());
            syntax.push_str(")");
            if n.value().loop_marker {
              syntax.push_str(" Loop");
            }
            syntax.push_str("\\n");
            syntax.push_str(n.value().requested.replace(">", "#gt;").replace("<", "#lt;").as_str());
            syntax.push_str(" \u{279C} ");
            syntax.push_str(n.value().resolved.as_str());

            syntax.push_str("\", shape: ");
            if node_has_child && id_path.is_empty() {
              // project
              syntax.push_str("card }\n");
              syntax.push_str(indent.as_str());
              syntax.push_str("class ");
              syntax.push_str(node_name.as_str());
              syntax.push_str(" primary\n");
            } else if node_has_child {
              // container node
              syntax.push_str("odd }\n");
              syntax.push_str(indent.as_str());
              syntax.push_str("class ");
              syntax.push_str(node_name.as_str());
              syntax.push_str(" secondary\n");
            } else {
              // edge node
              if n.value().loop_marker {
                syntax.push_str("processes }\n");
              } else {
                syntax.push_str("hexagon }\n");
              }
              syntax.push_str(indent.as_str());
              syntax.push_str("class ");
              syntax.push_str(node_name.as_str());
              if n.value().loop_marker {
                syntax.push_str(" looped\n");
              } else {
                if n.value().resolved == "UNRESOLVED" {
                  syntax.push_str(" unresolved\n");
                } else {
                  syntax.push_str(" tertiary\n");
                }
              }
            }

            if !id_path.is_empty() {
              let last_id = id_path.last().unwrap();
              let mut link = String::new();
              link.push_str(last_id.as_str());
              link.push_str(" -->| - | ");
              if made_sub_node {
                link.push_str(sub_graph_path.last().unwrap());
              } else {
                link.push_str(node_name.as_str());
              }
              link.push_str("\n");
              links.push(link.clone());
            }
          }
          if add_path {
            id_path.push(node_name.clone());
          }
          if remove_path {
            id_path.pop();
            sub_graph_path.pop();
            indent = " ".repeat(sub_graph_path.len() * 2);
            if !id_path.is_empty() {
              syntax.push_str(indent.as_str());
              syntax.push_str("end\n");
            }
          }
        },
        None => {},
      }
    }
    let link_str = links.join("");
    syntax.push_str(&link_str);
    syntax
  }

  fn load_in_browser(&self, file: PathBuf) {
    let res = self
      .tab
      .navigate_to(&format!("file:///{}", file.into_os_string().to_str().unwrap()))
      .unwrap()
      .wait_for_xpath_with_custom_timeout("//*[@id=\"mermaid-content\"]", Duration::from_secs(600));
    match res {
      Ok(_) => {},
      Err(_) => {},
    }
  }

  fn get_svg_element(&self) -> Option<String> {
    let res = self.tab.wait_for_xpath_with_custom_timeout("//*[@id=\"mermaid-content\"]", Duration::from_secs(600));
    match res {
      Ok(ele) => Some(ele.get_content().unwrap().replace("<br>", "<br/>")),
      Err(_) => None,
    }
  }

  fn get_loaded_html_doc(&self) -> Option<String> {
    let res = self.tab.get_content();
    match res {
      Ok(ele) => Some(ele.replace("<br>", "<br/>")),
      Err(_) => None,
    }
  }
}

pub struct Report {
  reports: Vec<ExportType>,
  report_name: String,
  tree: DependencyTree,
  report_dir: PathBuf,
  mermaid_css: String,
  messages: Vec<(String, bool)>,
  html_created: bool,
}

impl Report {
  pub fn new(types: Vec<ExportType>, report_name: String, abs_dir: PathBuf, tree: DependencyTree) -> Self {
    Report {
      reports: types,
      report_name,
      tree,
      report_dir: abs_dir,
      mermaid_css: String::new(),
      messages: Vec::new(),
      html_created: false,
    }
  }

  pub fn generate(&mut self, progress_bar: Option<&ProgressBar>) -> Vec<(String, bool)> {
    let reports = self.reports.clone();
    for report in reports {
      match report {
        // None
        ExportType::None => (),
        // Uses Mermaid
        ExportType::Html => self.generate_html(progress_bar),
        // Static function
        ExportType::HtmlTree => self.generate_html_tree(progress_bar),
        // Static function
        ExportType::TxtTree => self.generate_text_tree(progress_bar),
        // Uses Mermaid
        ExportType::Mermaid => self.generate_mermaid(progress_bar),
        // Static function
        ExportType::TxtList => self.generate_text_list(progress_bar),
        // Uses Mermaid
        ExportType::Svg => self.generate_svg(progress_bar),
        // Uses Mermaid
        ExportType::Png => self.generate_png(progress_bar),
        // Uses Mermaid
        ExportType::Jpg => self.generate_jpg(progress_bar),
        // Uses Mermaid
        ExportType::Md => self.generate_md(progress_bar),
        // Static function
        ExportType::Xml => self.generate_xml(progress_bar),
        // Uses Mermaid
        ExportType::StaticHtml => self.generate_static_html(progress_bar),
      }
    }
    // Cleanup unneeded reports
    self.cleanup_html();
    self.messages.clone()
  }

  fn cleanup_html(&self) {
    if !self.reports.contains(&ExportType::Html) {
      let path = &self.report_dir.join(PathBuf::from(&self.report_name).with_extension("html"));
      if path.exists() {
        std::fs::remove_file(path).unwrap();
      }
    }
  }

  fn write(&self, suffix: &str, content: &str) {
    let path = &self.report_dir.join(PathBuf::from(&self.report_name).with_extension(suffix));
    write_generic_file(path.clone(), content.to_string());
  }

  fn write_bytes(&self, suffix: &str, content: &Vec<u8>) {
    let path = &self.report_dir.join(PathBuf::from(&self.report_name).with_extension(suffix));
    write_generic_file_bytes(path.clone(), content.as_slice());
  }

  fn start_mermaid_instance(&self) -> Mermaid {
    Mermaid::new(self.tree.get_root().value().name.clone())
  }

  fn load_mermaid_in_browser(&self, mermaid: &Mermaid) {
    let path = &self.report_dir.join(PathBuf::from(&self.report_name).with_extension("html"));
    mermaid.load_in_browser(path.clone());
  }

  fn generate_html(&mut self, progress_bar: Option<&ProgressBar>) {
    if !self.html_created {
      Timer.start("Generating Html Report", !self.reports.contains(&ExportType::Html), progress_bar.is_none());
      let mermaid = self.start_mermaid_instance();
      self.mermaid_css = mermaid.mermaid_css.clone();
      let syntax = mermaid.generate_mermaid_syntax(self.tree.clone());
      let mermaid_html = mermaid.generate_html(syntax);
      self.write("html", &mermaid_html);
      self.html_created = true;
      if progress_bar.is_some() {
        self.messages.push(Timer.stop_and_return_message("Generating Html Report"));
      } else {
        Timer.stop("Generating Html Report");
      }
    }
  }

  fn generate_static_html(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start(
      "Generating Static Html Report",
      !self.reports.contains(&ExportType::StaticHtml),
      progress_bar.is_none(),
    );
    self.generate_html(progress_bar);
    let mermaid = self.start_mermaid_instance();
    self.load_mermaid_in_browser(&mermaid);
    let mermaid_html = mermaid.get_loaded_html_doc();
    if mermaid_html.is_some() {
      let doc = mermaid_html.unwrap();
      let html = Document::from(doc);
      html.select("#loading-info").remove();
      html.select("script").remove();
      self.write("static.html", html.html().to_string().as_str());
    }
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Static Html Report"));
    } else {
      Timer.stop("Generating Static Html Report");
    }
  }

  fn generate_mermaid(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Mermaid Report", !self.reports.contains(&ExportType::Mermaid), progress_bar.is_none());
    let mermaid = self.start_mermaid_instance();
    let syntax = mermaid.generate_mermaid_syntax(self.tree.clone());
    self.write("mermaid", &syntax);
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Mermaid Report"));
    } else {
      Timer.stop("Generating Mermaid Report");
    }
  }

  fn generate_svg(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Svg Report", !self.reports.contains(&ExportType::Svg), progress_bar.is_none());
    self.generate_html(progress_bar);
    let mermaid = self.start_mermaid_instance();
    self.load_mermaid_in_browser(&mermaid);
    let ele = mermaid.get_svg_element();
    match ele {
      Some(element) => {
        self.write("svg", element.as_str());
      },
      None => {},
    }
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Svg Report"));
    } else {
      Timer.stop("Generating Svg Report");
    }
  }

  fn pre_image_svg(&mut self, progress_bar: Option<&ProgressBar>) -> Option<String> {
    self.generate_html(progress_bar);
    let mermaid = self.start_mermaid_instance();
    self.load_mermaid_in_browser(&mermaid);
    let ele = mermaid.get_svg_element();
    ele
  }

  fn generate_png(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Png Report", !self.reports.contains(&ExportType::Png), progress_bar.is_none());
    let ele = self.pre_image_svg(progress_bar);
    match ele {
      Some(element) => {
        let pixmap = self.svg_convert(element.into_bytes(), self.mermaid_css.clone());
        self.write_bytes("png", &pixmap)
      },
      None => {},
    }
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Png Report"));
    } else {
      Timer.stop("Generating Png Report");
    }
  }

  fn generate_jpg(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Jpeg Report", !self.reports.contains(&ExportType::Jpg), progress_bar.is_none());
    let ele = self.pre_image_svg(progress_bar);
    match ele {
      Some(element) => {
        let png_encode = self.svg_convert(element.into_bytes(), self.mermaid_css.clone());
        let image_buf = self.png_to_dynamic_image(png_encode);
        let mut jpeg_buf = Vec::new();
        let mut jpeg_encoder = JpegEncoder::new_with_quality(&mut jpeg_buf, 100);
        jpeg_encoder.encode_image(&image_buf).expect("TODO: panic message");
        self.write_bytes("jpg", &jpeg_buf)
      },
      None => {},
    }
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Jpeg Report"));
    } else {
      Timer.stop("Generating Jpeg Report");
    }
  }

  fn generate_md(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Md Report", !self.reports.contains(&ExportType::Md), progress_bar.is_none());
    let mermaid = self.start_mermaid_instance();
    let syntax = mermaid.generate_mermaid_syntax(self.tree.clone());
    let mut md_file = String::new();
    md_file.push_str("```mermaid\n");
    md_file.push_str(syntax.as_str());
    md_file.push_str("\n```");
    self.write("md", &md_file);
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Md Report"));
    } else {
      Timer.stop("Generating Md Report");
    }
  }

  fn generate_text_tree(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Text Tree Report", !self.reports.contains(&ExportType::TxtTree), progress_bar.is_none());
    let tree = self.tree.get_tree_string();
    self.write("text-tree.txt", &tree);
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Text Tree Report"));
    } else {
      Timer.stop("Generating Text Tree Report");
    }
  }

  fn generate_text_list(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Text List Report", !self.reports.contains(&ExportType::TxtList), progress_bar.is_none());
    let mut content: Vec<String> = Vec::new();
    let mut iter = self.tree.get_root().descendants().peekable();
    while iter.peek().is_some() {
      let item = iter.next().unwrap();
      content.push(item.value().to_string());
    }
    content.swap_remove(0);
    content = content.into_iter().filter(|item| !item.ends_with(" LOOP")).collect();
    content.sort();
    content.dedup();
    self.write("text-list.txt", &content.join("\n"));
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Text List Report"));
    } else {
      Timer.stop("Generating Text List Report");
    }
  }

  fn generate_html_tree(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Html Tree Report", !self.reports.contains(&ExportType::HtmlTree), progress_bar.is_none());
    let tree = self.tree.get_tree_string();
    let mut content = String::new();
    content.push_str("<html><pre>");
    content.push_str(&tree);
    content.push_str("</pre></html>");
    self.write("html-tree.html", &content);
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Html Tree Report"));
    } else {
      Timer.stop("Generating Html Tree Report");
    }
  }

  fn generate_xml(&mut self, progress_bar: Option<&ProgressBar>) {
    Timer.start("Generating Xml Report", !self.reports.contains(&ExportType::Xml), progress_bar.is_none());
    let mut iter = self.tree.get_root().descendants().peekable();
    let mut content: Vec<String> = Vec::new();
    content.push("<dependencies>".to_string());
    while iter.peek().is_some() {
      let item = iter.next().unwrap();
      let mut entry = String::new();
      if item.parent().is_some() {
        if item.parent().unwrap().first_child().unwrap().value().name == item.value().name {
          entry.push_str("<dependency-group>");
        }
      } else {
        continue;
      }
      entry.push_str("<dependency requested=\"");
      entry.push_str(escape(item.value().requested.as_str()).to_string().as_str());
      entry.push_str("\" resolved=\"");
      entry.push_str(escape(item.value().resolved.as_str()).to_string().as_str());
      entry.push_str("\" name=\"");
      entry.push_str(escape(item.value().name.as_str()).to_string().as_str());
      entry.push_str("\" loop=\"");
      entry.push_str(escape(item.value().loop_marker.to_string()).to_string().as_str());
      entry.push_str("\" type=\"");
      entry.push_str(escape(item.value().kind.to_string()).to_string().as_str());
      entry.push_str("\" />");
      if item.parent().is_some() {
        if item.parent().unwrap().last_child().unwrap().value().name == item.value().name {
          entry.push_str("</dependency-group>");
        }
      }
      content.push(entry);
    }
    content.push("</dependencies>".to_string());
    self.write("xml", &content.join("\n"));
    if progress_bar.is_some() {
      self.messages.push(Timer.stop_and_return_message("Generating Xml Report"));
    } else {
      Timer.stop("Generating Xml Report");
    }
  }

  fn svg_convert(&mut self, svg_data: Vec<u8>, css: String) -> Vec<u8> {
    let tree = {
      let mut opt = usvg::Options::default();
      // Get file's absolute directory.
      opt.resources_dir =
        std::fs::canonicalize(&self.report_dir).ok().and_then(|p| p.parent().map(|p| p.to_path_buf()));

      opt.font_family = "Verdana".to_string();
      opt.font_size = 11.0;
      opt.dpi = 300.0;
      opt.font_resolver = FontResolver::default();
      opt.shape_rendering = ShapeRendering::GeometricPrecision;
      opt.text_rendering = TextRendering::GeometricPrecision;
      opt.image_rendering = ImageRendering::OptimizeQuality;
      opt.style_sheet = Some(css);

      opt.fontdb_mut().load_system_fonts();

      usvg::Tree::from_data(&svg_data, &opt).unwrap()
    };
    let pixmap_size = tree.size().to_int_size();
    let mut pixmap = tiny_skia::Pixmap::new(pixmap_size.width(), pixmap_size.height()).unwrap();
    pixmap.fill(Color::from_rgba8(223, 222, 231, 100));
    resvg::render(&tree, tiny_skia::Transform::default(), &mut pixmap.as_mut());
    pixmap.encode_png().unwrap()
  }

  fn png_to_dynamic_image(&self, png_data: Vec<u8>) -> DynamicImage {
    let buf = image::load_from_memory(png_data.as_slice()).unwrap();
    buf
  }
}

use crate::cli::Out;
use chrono::{DateTime, Utc};
use lockable::{LockableHashMap, SyncLimit};
use static_init::dynamic;

#[dynamic(lazy)]
pub static Timer: Timing = Timing::new();

pub struct Timing {
  timers: LockableHashMap<String, (DateTime<Utc>, bool)>,
}
impl Timing {
  pub fn new() -> Self {
    Timing {
      timers: LockableHashMap::new(),
    }
  }

  pub fn start(&self, name: &str, debug: bool, show: bool) {
    let guard = self.timers.blocking_lock(name.to_string(), SyncLimit::no_limit());
    if guard.is_ok() {
      let mut guard = guard.unwrap();
      guard.insert((Utc::now(), debug));
      if debug {
        Out.write_debug(name);
      } else {
        if show {
          Out.write_info(name);
        }
      }
    }
  }

  pub fn stop(&self, name: &str) {
    let now = Utc::now();
    let guard = self.timers.blocking_lock(name.to_string(), SyncLimit::no_limit());
    if guard.is_ok() {
      let mut guard = guard.unwrap();
      let start_opts = guard.value().unwrap().clone();
      let start = start_opts.0;
      let duration = now - start;
      let message = format!("{} took", name);
      let mut message_time = format!("{}ms", duration.num_milliseconds());
      if duration.num_milliseconds() > 1000 {
        message_time = format!("{}.{}s", duration.num_seconds(), duration.subsec_nanos().ilog10());
      }
      if start_opts.1 {
        Out.write_debug(&Out.colorize(&message, "green", &message_time));
      } else {
        Out.write_info(&Out.colorize(&message, "green", &message_time));
      }
      guard.remove();
    }
  }

  pub fn stop_and_return_message(&self, name: &str) -> (String, bool) {
    let now = Utc::now();
    let guard = self.timers.blocking_lock(name.to_string(), SyncLimit::no_limit());
    if guard.is_ok() {
      let mut guard = guard.unwrap();
      let start_opts = guard.value().unwrap().clone();
      let start = start_opts.0;
      let duration = now - start;
      let message = format!("{} took", name);
      let mut message_time = format!("{}ms", duration.num_milliseconds());
      if duration.num_milliseconds() > 1000 {
        message_time = format!("{}.{}s", duration.num_seconds(), duration.subsec_nanos().ilog10());
      }
      let msg = Out.colorize(message.as_str(), "green", message_time.as_str());
      guard.remove();
      return (msg, start_opts.1);
    }
    ("".to_string(), true)
  }
}

use crate::cli::Out;
use ego_tree::{NodeId, NodeRef, Tree};

#[derive(Clone)]
pub struct TreeNodeId(NodeId);

impl std::fmt::Display for TreeNodeId {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    let dbg = format!("{:?}", self.0);
    let dbg_parts = dbg.split(['(', ')']).filter(|s| s.parse::<usize>().is_ok()).collect::<Vec<_>>();
    write!(f, "{}", dbg_parts.get(0).unwrap())
  }
}

impl TreeNodeId {
  pub fn new(node: NodeId) -> TreeNodeId {
    TreeNodeId(node)
  }
}

#[derive(PartialEq, Copy, Clone)]
pub enum DependencyKind {
  Required,
  Development,
  Peer,
  PeerOptional,
  Root,
}

impl std::fmt::Display for DependencyKind {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    match self {
      DependencyKind::Required => write!(f, "Required"),
      DependencyKind::Development => write!(f, "Development"),
      DependencyKind::Peer => write!(f, "Peer"),
      DependencyKind::PeerOptional => write!(f, "Peer Optional"),
      DependencyKind::Root => write!(f, "Root"),
    }
  }
}

impl DependencyKind {
  pub fn to_short_string(&self) -> String {
    match self {
      DependencyKind::Required => "R".to_string(),
      DependencyKind::Development => "D".to_string(),
      DependencyKind::Peer => "P".to_string(),
      DependencyKind::PeerOptional => "PO".to_string(),
      DependencyKind::Root => "PR".to_string(),
    }
  }
}

#[derive(Clone)]
pub struct Dependency {
  pub name: String,
  pub source: String,
  pub requested: String,
  pub resolved: String,
  pub kind: DependencyKind,
  pub processed: bool,
  pub loop_marker: bool,
}

impl std::fmt::Display for Dependency {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    if self.loop_marker {
      write!(f, "{} ({} \u{279C} {}) - {} \u{2B6F} LOOP", self.name, self.requested, self.resolved, self.kind)
    } else {
      write!(f, "{} ({} \u{279C} {}) - {}", self.name, self.requested, self.resolved, self.kind)
    }
  }
}

impl Dependency {
  pub fn new(
    name: String,
    source: String,
    requested: String,
    resolved: String,
    kind: DependencyKind,
    is_loop: bool,
  ) -> Self {
    Dependency {
      name,
      source,
      requested,
      resolved,
      kind,
      processed: false,
      loop_marker: is_loop,
    }
  }
}

#[derive(Clone)]
pub struct DependencyTree {
  tree: Tree<Dependency>,
  last_processed_parent: Option<NodeId>,
}

impl DependencyTree {
  pub fn new(name: String, version: String) -> Self {
    let rd = Dependency::new(name.clone(), name.clone(), version.clone(), version.clone(), DependencyKind::Root, false);
    let t = Tree::new(rd);
    DependencyTree {
      tree: t,
      last_processed_parent: None,
    }
  }

  pub fn print_node(&self, node_id: NodeId) {
    let tree_node = TreeNodeId::new(node_id);
    let node = self.tree.get(node_id);
    if node.is_some() {
      let node = node.unwrap();
      Out.write_trace(&format!("Node #{} {{", &tree_node.to_string()));
      Out.write_trace(&format!("  Name: {}", &node.value().name));
      Out.write_trace(&format!("  Kind: {}", &node.value().kind));
      Out.write_trace(&format!("  Source: {}", &node.value().source));
      Out.write_trace(&format!("  Requested: {}", &node.value().requested));
      Out.write_trace(&format!("  Resolved: {}", &node.value().resolved));
      Out.write_trace(&format!("  Processed: {}", &node.value().processed));
      Out.write_trace(&format!("  Children: {}", &node.children().count()));
      Out.write_trace("}");
    } else {
      Out.write_error(&Out.colorize("Node not found:", "yellow", &tree_node.to_string()), true);
      panic!()
    }
  }

  pub fn print_tree(&self, enabled: bool) {
    if enabled {
      let tree_string = format!("{}", &self.tree)
        .split_terminator('\n')
        .map(|s| {
          let mut n = String::from("  ");
          n.push_str(s);
          return n;
        })
        .collect::<Vec<String>>();
      Out.write_debug("Tree:");
      tree_string.iter().for_each(|s| Out.write_debug(&s));
    }
  }

  pub fn get_tree_string(&self) -> String {
    let tree_string = format!("{}", &self.tree);
    tree_string.clone()
  }

  pub fn get_node_count(&self) -> usize {
    self.tree.root().descendants().count()
  }

  pub fn get_node(&self, node_id: NodeId) -> Option<NodeRef<Dependency>> {
    self.tree.get(node_id)
  }

  pub fn get_root(&self) -> NodeRef<Dependency> {
    self.tree.root().clone()
  }

  pub fn get_parent_id(&self, node_id: NodeId) -> Option<NodeId> {
    let n = self.tree.get(node_id);
    if n.is_some() {
      let n2 = n.unwrap();
      if n2.parent().is_some() {
        return Some(n2.parent().unwrap().id());
      }
    }
    None
  }

  pub fn add_root(&mut self, version: String) -> NodeId {
    let mut proj = self.tree.root_mut();
    proj.value().resolved = version.clone();
    proj.value().requested = version.clone();
    proj.id()
  }

  pub fn add_dependency(
    &mut self,
    parent: NodeId,
    name: String,
    source: String,
    requested: String,
    resolved: String,
    kind: DependencyKind,
  ) -> Option<NodeId> {
    let parent_node = self.tree.get(parent);
    if parent_node.is_some() {
      let parent_node = parent_node.unwrap();
      if parent_node.value().loop_marker {
        return None;
      }
      let mut is_loop = false;
      if parent_node.value().name == name
        && parent_node.value().resolved == resolved
        && parent_node.value().requested == requested
      {
        is_loop = true;
      }
      if !is_loop {
        let exists_above = parent_node
          .ancestors()
          .find(|n| n.value().name == name && n.value().resolved == resolved && n.value().requested == requested);
        if exists_above.is_some() {
          is_loop = true;
        } else {
          let parent_parent_node = parent_node.parent();
          let mut exists_around = None;
          if parent_parent_node.is_some() {
            exists_around = parent_parent_node
              .unwrap()
              .descendants()
              .find(|n| n.value().name == name && n.value().resolved == resolved && n.value().requested == requested);
          }
          if exists_around.is_some() {
            is_loop = true;
            //return None;
          }
        }
      }

      let mut parent_node_mut = self.tree.get_mut(parent).unwrap();
      let new_node = Dependency::new(name, source, requested, resolved, kind, is_loop);
      let node = parent_node_mut.append(new_node);
      Some(node.id())
    } else {
      let tree_node = TreeNodeId::new(parent);
      Out.write_error(&Out.colorize("Parent node not found:", "yellow", &tree_node.to_string()), true);
      None
    }
  }

  pub fn set_processed(&mut self, node_id: NodeId) {
    let node = self.tree.get_mut(node_id);
    if node.is_some() {
      let mut node = node.unwrap();
      node.value().processed = true;
    }
  }

  pub fn get_next_unprocessed(&mut self) -> Option<NodeId> {
    let mut item: Option<NodeRef<Dependency>>;
    if self.last_processed_parent.is_none() {
      item = self.tree.root().descendants().find(|i| !i.value().processed);
    } else {
      item = self.tree.get(self.last_processed_parent.unwrap()).unwrap().descendants().find(|i| !i.value().processed);
    }
    if item.is_none() && self.last_processed_parent.is_some() {
      item = self.tree.root().descendants().find(|i| !i.value().processed);
    }
    if item.is_none() && self.last_processed_parent.is_none() {
      return None;
    }

    if item.is_some() {
      let item = item.unwrap();
      if item.parent().is_some() {
        self.last_processed_parent = Some(item.parent().unwrap().id());
      } else {
        self.last_processed_parent = None;
      }
      return Some(item.id());
    }
    None
  }
}

#![allow(dead_code)]
#![allow(non_upper_case_globals)]
mod cli;
mod dependency;
mod file_io;
mod report;
mod utils;

use crate::cli::*;
use crate::dependency::*;
use crate::file_io::*;
use crate::report::Report;
use crate::utils::Timer;
use ego_tree::NodeId;
use indicatif::ProgressBar;
use json::JsonValue;
use std::fs;
use std::path::PathBuf;
use std::process::Command;

fn main() {
  // Start of execution
  Out.write_info("Starting Sketti");
  Timer.start("Execution", false, false);

  // Parse CLI args
  let cli = Cli::new();
  cli.validate();

  // Project Loading and Validation
  Timer.start("Loading project files", false, *DebugMode.read() != 0);
  let abs_dir = cli.abs_directory.to_str().unwrap();
  let project_json = open_json_file(cli.package_json.to_str().unwrap());
  if cli.yarn_mode && !cli.package_lock.exists() {
    prepare_yarn_repo(&cli.abs_directory, &cli.package_lock);
  }
  let project_lock = consolidate_lock_file(&open_json_file(cli.package_lock.to_str().unwrap()));
  Timer.stop("Loading project files");

  // Processing Dependencies
  // Make Tree
  Timer.start("Creating dependency tree", false, *DebugMode.read() != 0);
  let proj_name = project_json["name"].as_str().unwrap().to_string();
  let proj_version = project_json["version"].as_str().unwrap().to_string();
  let mut tree = DependencyTree::new(proj_name.clone(), proj_version);
  Timer.stop("Creating dependency tree");

  // Setup Output and loop
  Timer.start("Processing Dependencies", false, *DebugMode.read() != 0);
  let mut process_bar: Option<ProgressBar> = None;
  if *DebugMode.read() == 0 {
    process_bar = Some(Out.show_progress("Processing Dependencies".to_string()));
  }

  // Loop through dependencies until all are processed
  let mut loop_iterations: u64 = 0;
  loop {
    // Get next to process. None == end
    let next_id = tree.get_next_unprocessed();
    if next_id.is_none() {
      Out.write_debug("Completed processing");
      break;
    } else {
      loop_iterations += 1;
      // Debug to stop early
      // if loop_iterations > 10000 {
      //     break;
      // }

      // Create Reused variables
      let next_id = next_id.unwrap();
      let node_ref = tree.get_node(next_id).unwrap();
      let node = node_ref.value();
      let node_kind = node.kind.to_owned();
      let node_name = node.name.to_owned();
      let dev_deps: Vec<String>;
      let peer_deps: Vec<String>;
      let opt_peer_deps: Vec<String>;
      let req_deps: Vec<String>;
      let mut lock_node: Option<String> = None;
      let mut node_pkg_json_path = String::new();
      let node_pkg_json;

      // Top of loop outputting
      Out.write_verbose(&Out.colorize("Loop iteration", "red", loop_iterations.to_string().as_str()));
      let timer_name = format!("Processing NodeId: {}", TreeNodeId::new(next_id).to_string());
      Timer.start(&timer_name, true, false);
      Out.write_verbose(&Out.colorize("Processing node", "cyan", node.to_string().as_str()));

      // Process and fill variables based on item and parent
      if node_kind != DependencyKind::Root {
        if node_name != proj_name {
          lock_node = Some(node_name.clone());
        }
        let node_parent = (&node_ref.parent().unwrap().value().name).to_string();
        if node_parent != proj_name {
          Out.write_trace(&format!("lock_parent: {}", node_name.clone()));
          Out.write_trace(&format!("lock_parent_parent: {}", node_parent));
        }
      }
      let lock_entry = get_lock_entry(
        &project_lock,
        node_name.as_str(),
        lock_node.as_deref(),
        node_ref,
        node.kind,
        &abs_dir,
        &proj_name,
      );
      if lock_entry.is_some() {
        node_pkg_json_path = get_pkg_json_path(lock_entry.unwrap().0.as_str(), abs_dir);
      }
      if node_kind == DependencyKind::Root {
        node_pkg_json = project_json.clone();
      } else {
        node_pkg_json = open_json_file(node_pkg_json_path.as_str());
      }

      // Get dependency lists
      req_deps = get_pkg_dependencies(&node_pkg_json, DependencyKind::Required);
      peer_deps = get_pkg_dependencies(&node_pkg_json, DependencyKind::Peer);
      opt_peer_deps = get_pkg_dependencies(&node_pkg_json, DependencyKind::PeerOptional);

      // Process required dependencies
      for dep in req_deps {
        process_dependency(
          dep,
          DependencyKind::Required,
          &mut tree,
          &project_lock,
          &node_pkg_json,
          next_id,
          &lock_node,
          &abs_dir,
          &proj_name,
        );
      }

      // Process peer dependencies
      for dep in peer_deps {
        process_dependency(
          dep,
          DependencyKind::Peer,
          &mut tree,
          &project_lock,
          &node_pkg_json,
          next_id,
          &lock_node,
          &abs_dir,
          &proj_name,
        );
      }

      // Process optional peer dependencies
      for dep in opt_peer_deps {
        process_dependency(
          dep,
          DependencyKind::PeerOptional,
          &mut tree,
          &project_lock,
          &node_pkg_json,
          next_id,
          &lock_node,
          &abs_dir,
          &proj_name,
        );
      }

      // Process development dependencies
      if node_kind == DependencyKind::Root && !cli.release_only {
        dev_deps = get_pkg_dependencies(&node_pkg_json, DependencyKind::Development);
        for dep in dev_deps {
          process_dependency(
            dep,
            DependencyKind::Development,
            &mut tree,
            &project_lock,
            &node_pkg_json,
            next_id,
            &lock_node,
            &abs_dir,
            &proj_name,
          );
        }
      }

      // Set current item as complete
      tree.set_processed(next_id);
      Timer.stop(&timer_name);
    }
  }

  // Post loop outputting
  if *DebugMode.read() == 0 && process_bar.is_some() {
    Out.end_progress(process_bar.unwrap(), true);
  }
  Timer.stop("Processing Dependencies");
  Out.write_info(&Out.colorize("Total Dependency Relationships", "yellow", tree.get_node_count().to_string().as_str()));

  // Start of report generation
  Timer.start("Processing Reports", false, *DebugMode.read() != 0);
  let mut report_bar: Option<ProgressBar> = None;
  if *DebugMode.read() == 0 {
    report_bar = Some(Out.show_progress("Processing Reports".to_string()));
  }

  // Generate reports
  let mut report = Report::new(cli.report_type, cli.report_name, cli.abs_directory, tree.clone());
  let messages = report.generate(report_bar.as_ref());

  // Post report generation messaging
  if *DebugMode.read() == 0 && report_bar.is_some() {
    Out.end_progress(report_bar.unwrap(), true);
  }
  for message in messages {
    if message.1 {
      Out.write_debug(&message.0);
    } else {
      Out.write_info(&message.0);
    }
  }
  Timer.stop("Processing Reports");

  // Show tree if enabled
  tree.print_tree(cli.show_tree.clone());

  // Yarn specific cleanup
  if cli.yarn_mode {
    if cli.package_lock.exists() {
      fs::remove_file(cli.package_lock).unwrap();
    }
  }

  // Final output messaging
  Timer.stop("Execution");
  Out.write_info("Sketti Complete");
}

/// Prepares a yarn project for use with this tool.
/// Creates a npm-shrinkwrap.json and renames to package-lock.json
fn prepare_yarn_repo(dir: &PathBuf, pkg_lock: &PathBuf) {
  Out.write_info("Creating package-lock.json from yarn project.");
  let npm_output = Command::new("npm.cmd").args(&["shrinkwrap"]).current_dir(dir.to_str().unwrap()).output();
  if npm_output.is_ok() {
    let shrinkwrap_path = PathBuf::from(dir).join("npm-shrinkwrap.json");
    let rename = fs::rename(shrinkwrap_path, pkg_lock);
    if rename.is_err() || !pkg_lock.exists() {
      Out.write_error(
        "Failed to generate package-lock.json for yarn support. File not found after successful npm command",
        true,
      );
    }
  } else {
    Out.write_error("Failed to generate package-lock.json for yarn support", true);
  }
}

/// Processes a dependency from the loops in main.
fn process_dependency(
  dependency_name: String,
  dependency_type: DependencyKind,
  tree: &mut DependencyTree,
  project_lock: &JsonValue,
  pkg_json: &JsonValue,
  node_id: NodeId,
  lock_node: &Option<String>,
  abs_dir: &str,
  proj_name: &str,
) {
  let node = tree.get_node(node_id).unwrap();
  let node_parent_id = node.id();
  let node_name = node.value().name.to_owned();

  let req_ver = get_requested_version(dependency_name.as_str(), pkg_json, dependency_type);
  let dep_lock_entry = get_lock_entry(
    project_lock,
    dependency_name.as_str(),
    lock_node.as_deref(),
    node,
    dependency_type,
    abs_dir,
    proj_name,
  );
  let new_id: Option<NodeId>;
  let mut unresolved = false;
  match dep_lock_entry {
    None => {
      new_id = tree.add_dependency(
        node_parent_id,
        dependency_name,
        node_name,
        req_ver,
        "UNRESOLVED".to_string(),
        dependency_type,
      );
      unresolved = true;
    },
    Some(dep_lock_entry) => {
      let dep_resolved = dep_lock_entry.1;
      new_id = tree.add_dependency(node_parent_id, dependency_name, node_name, req_ver, dep_resolved, dependency_type);
    },
  }
  if new_id.is_some() {
    let new_tree_node = TreeNodeId::new(new_id.unwrap());
    Out.write_trace(&Out.colorize("Created node ", "green", new_tree_node.to_string().as_str()));
    if unresolved {
      tree.set_processed(new_id.unwrap());
    }
    if &tree.get_node(new_id.unwrap()).unwrap().ancestors().count() >= DepthMax.get().unwrap() {
      tree.set_processed(new_id.unwrap());
    }
  }
}

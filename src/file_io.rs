use crate::cli::{Filter, Out};
use crate::dependency::{Dependency, DependencyKind};
use ego_tree::NodeRef;
use json::JsonValue;
use std::error::Error;
use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};

/// Opens a json file from a path
///
/// # Example
/// ```no_run
/// let json = open_json_file("S:\\some_file.json");
/// ```
pub fn open_json_file(file: &str) -> JsonValue {
  let data = open_file(file);
  if data.is_some() {
    let json = convert_to_json(&data.unwrap());
    json
  } else {
    JsonValue::Null
  }
}

fn open_file(file: &str) -> Option<String> {
  let f = File::open(file);
  match f {
    Ok(mut contents) => {
      let mut content = String::new();
      contents.read_to_string(&mut content).unwrap();
      Some(content)
    },
    Err(err) => {
      Out.write_error(&err.to_string(), false);
      Out.write_error(&err.source()?.to_string(), true);
      None
    },
  }
}

fn convert_to_json(data: &str) -> JsonValue {
  let json = json::parse(data);
  json.unwrap_or_else(|err| {
    Out.write_error(&err.to_string(), true);
    JsonValue::Null
  })
}

pub fn consolidate_lock_file(lock_file: &JsonValue) -> JsonValue {
  let mut new_lock_file = JsonValue::new_object();
  new_lock_file["packages"] = JsonValue::new_object();
  for entry in lock_file["packages"].entries() {
    new_lock_file["packages"][entry.0] = JsonValue::new_object();
    new_lock_file["packages"][entry.0]["version"] = JsonValue::from(entry.1["version"].as_str());
    if lock_file["packages"][entry.0].has_key("peer") {
      new_lock_file["packages"][entry.0]["peer"] = JsonValue::from(entry.1["peer"].as_bool());
    }
    if lock_file["packages"][entry.0].has_key("dev") {
      new_lock_file["packages"][entry.0]["dev"] = JsonValue::from(entry.1["dev"].as_bool());
    }
  }
  new_lock_file
}

pub fn get_lock_entry(
  lock_file: &JsonValue,
  pkg_name: &str,
  node: Option<&str>,
  node_item: NodeRef<Dependency>,
  dep_kind: DependencyKind,
  abs_dir: &str,
  proj_name: &str,
) -> Option<(String, String)> {
  let mut node_path: String = String::from("node_modules/");
  if node.is_some() {
    node_path.push_str(node.unwrap());
    node_path.push_str("/node_modules/");
  }
  node_path.push_str(pkg_name);

  let mut root_path: String = String::from("node_modules/");
  root_path.push_str(pkg_name);

  let mut parent_paths: Vec<String> = Vec::new();
  let mut parent_paths_short: Vec<String> = Vec::new();
  if node_item.value().kind != DependencyKind::Peer || node_item.value().kind != DependencyKind::PeerOptional {
    let mut curr_node = node_item;
    while curr_node.parent().is_some() && curr_node.parent()?.value().name != "project" {
      let curr_path = parent_paths.last().unwrap_or(&root_path);
      let mut new_parent = String::from("node_modules/");
      new_parent.push_str(curr_node.parent().unwrap().value().name.as_str());
      new_parent.push_str("/");
      new_parent.push_str(curr_path);
      parent_paths.push(new_parent.clone());
      let mut new_parent_short = String::from("node_modules/");
      new_parent_short.push_str(curr_node.parent().unwrap().value().name.as_str());
      new_parent_short.push_str("/node_modules/");
      new_parent_short.push_str(pkg_name);
      parent_paths_short.push(new_parent_short.clone());
      curr_node = curr_node.parent().unwrap();
    }
  }
  parent_paths.reverse();
  parent_paths_short.dedup();
  parent_paths.append(&mut parent_paths_short);
  parent_paths.dedup();
  parent_paths.push(root_path.clone());

  let mut resolved = String::from("UNRESOLVED");
  let mut item: Option<(&str, &JsonValue)> = None;

  for parent in parent_paths.iter() {
    item = lock_file["packages"].entries().find(|(key, _val)| key == parent);
    if item.is_some() {
      resolved = parent.clone();
      break;
    }
  }

  if item.is_none() {
    item = lock_file["packages"].entries().find(|(key, _val)| key.ends_with(&node_path));
    if item.is_some() {
      resolved = node_path.clone();
    }
  }

  if item.is_none() {
    item = lock_file["packages"].entries().find(|(key, _val)| key == &root_path);
    if item.is_some() {
      resolved = root_path.clone();
    }
  }

  if item.is_none() {
    if pkg_name == proj_name {
      return None;
    }
    Out.write_trace("Lock entry paths:");
    Out.write_trace(&format!("  Root: {}", root_path.clone().replace("node_modules/", "")));
    Out.write_trace(&format!("  Node: {}", node_path.clone().replace("node_modules/", "")));
    Out.write_trace("  Node Parents:");
    for parent in parent_paths.iter() {
      Out.write_trace(&format!("    {}", parent.replace("node_modules/", "")));
    }

    if dep_kind == DependencyKind::Peer || dep_kind == DependencyKind::PeerOptional {
      Out.write_trace("  Resolved to: UNRESOLVED Peer Dependency");
      return None;
    }

    Out.write_trace(&format!("  Resolved to: {}", resolved));
    return None;
    //Out.write_error(format!("Error processing node dependency {}", pkg_name).as_str(), true);
  }

  if item.unwrap().1.has_key("peer") {
    if !PathBuf::from(get_pkg_json_path(item.unwrap().0, abs_dir)).exists() {
      return None;
    }
  }

  item.map(|(key, value)| (key.to_string(), value["version"].as_str().unwrap().to_string()))
}

pub fn get_pkg_json_path(pkg_name: &str, dir: &str) -> String {
  let path = Path::new(dir).join(pkg_name).join("package.json");
  path.to_str().unwrap().to_string()
}

pub fn get_requested_version(pkg_name: &str, pkg_json: &JsonValue, kind: DependencyKind) -> String {
  if kind == DependencyKind::Development {
    return pkg_json["devDependencies"][pkg_name].as_str().unwrap_or("UNRESOLVED").to_string();
  } else if kind == DependencyKind::Required {
    return pkg_json["dependencies"][pkg_name].as_str().unwrap_or("UNRESOLVED").to_string();
  } else if kind == DependencyKind::Peer || kind == DependencyKind::PeerOptional {
    return pkg_json["peerDependencies"][pkg_name].as_str().unwrap_or("UNRESOLVED").to_string();
  }
  "UNRESOLVED".to_string()
}

pub fn get_pkg_dependencies(pkg_json: &JsonValue, kind: DependencyKind) -> Vec<String> {
  let mut pkg_deps: Vec<String> = Vec::new();
  if kind == DependencyKind::Development {
    pkg_json["devDependencies"].entries().for_each(|item| pkg_deps.push(item.0.to_string()));
  } else if kind == DependencyKind::Required {
    pkg_json["dependencies"].entries().for_each(|item| pkg_deps.push(item.0.to_string()));
  } else if kind == DependencyKind::Peer {
    let mut temp_pkg_deps: Vec<String> = Vec::new();
    pkg_json["peerDependencies"].entries().for_each(|item| temp_pkg_deps.push(item.0.to_string()));
    if pkg_json.has_key("peerDependenciesMeta") {
      pkg_json["peerDependenciesMeta"].entries().for_each(|item| {
        if item.1.has_key("optional") && item.1["optional"] == true {
          temp_pkg_deps = temp_pkg_deps.clone().into_iter().filter(|d| d != item.0).collect();
        }
      });
    }
    pkg_deps = temp_pkg_deps;
  } else if kind == DependencyKind::PeerOptional {
    if pkg_json.has_key("peerDependenciesMeta") {
      pkg_json["peerDependenciesMeta"].entries().for_each(|item| {
        if item.1.has_key("optional") && item.1["optional"] == true {
          pkg_deps.push(item.0.to_string());
        }
      });
    }
  }
  if Filter.get().is_some() {
    let filtered: Vec<String> = pkg_deps.iter().filter(|&dep| Filter.get().unwrap().is_match(&dep)).cloned().collect();
    return filtered.clone();
  }
  pkg_deps.clone()
}

pub fn write_generic_file(file_path: PathBuf, content: String) {
  let mut file = File::create(file_path).unwrap();
  file.write_all(content.as_bytes()).unwrap();
  file.sync_all().unwrap();
}

pub fn write_generic_file_bytes(file_path: PathBuf, content: &[u8]) {
  let mut file = File::create(file_path).unwrap();
  file.write_all(content).unwrap();
  file.sync_all().unwrap();
}

### [2.11.0](https://gitlab.com/fidget-code/sketti/compare/2.10.0...2.11.0)

> 11 February 2025

### Commits

- Fix NPM publishing. @mediatech15 [`de2b90b`](https://gitlab.com/fidget-code/sketti/commit/de2b90b857d490f12d8bf4d339df437443ad48aa) *11 February 2025*
- Update README.md @mediatech15 [`5d45ae6`](https://gitlab.com/fidget-code/sketti/commit/5d45ae6382b0b2643cf7b5c8a8eddab48c71fde3) *21 January 2025*

### [2.10.0](https://gitlab.com/fidget-code/sketti/compare/2.9.0...2.10.0)

> 21 January 2025

- Add Depth Control [`#8`](https://gitlab.com/fidget-code/sketti/merge_requests/8)
- Improve report info [`#7`](https://gitlab.com/fidget-code/sketti/merge_requests/7)
- Improve Performance [`#6`](https://gitlab.com/fidget-code/sketti/merge_requests/6)

### CI Commits

- CI Version 2.10.0  [`293028b`](https://gitlab.com/fidget-code/sketti/commit/293028bcdaf26fb5b2713d378c7fad9858914a1f) *21 January 2025*
### [2.9.0](https://gitlab.com/fidget-code/sketti/compare/2.8.0...2.9.0)

> 21 January 2025

### CI Commits

- CI Version 2.9.0  [`1e63728`](https://gitlab.com/fidget-code/sketti/commit/1e637287c0de56505e86c36829f2ed24955f0fca) *21 January 2025*
- Fix bug in yarn mode for peer dependencies @mediatech15 [`5ee3d80`](https://gitlab.com/fidget-code/sketti/commit/5ee3d80a04cb591a80aed50c0251fbb9ec9bae25) *21 January 2025*
### [2.8.0](https://gitlab.com/fidget-code/sketti/compare/2.7.0...2.8.0)

> 20 January 2025

- Code cleanup and peer improvements [`#5`](https://gitlab.com/fidget-code/sketti/merge_requests/5)

### CI Commits

- CI Version 2.8.0  [`8f5bb23`](https://gitlab.com/fidget-code/sketti/commit/8f5bb23841d2aff4f407eaf2d7d4299e33f1dd67) *20 January 2025*
- Fix CI to not build schedule if changes to selected paths is not met. @mediatech15 [`a18e13e`](https://gitlab.com/fidget-code/sketti/commit/a18e13eb567b3eff0e0ee1a7abd879789fea077a) *20 January 2025*
### [2.7.0](https://gitlab.com/fidget-code/sketti/compare/2.6.0...2.7.0)

> 17 January 2025

### Commits

- Edit fix-changelog.sh @mediatech15 [`822886f`](https://gitlab.com/fidget-code/sketti/commit/822886f5562302935878c846e3aa886758d53ed7) *17 January 2025*

### CI Commits

- CI Version 2.7.0  [`273cfcd`](https://gitlab.com/fidget-code/sketti/commit/273cfcd63fd10e8adae44481dd558e42f6d8a4a1) *17 January 2025*
### [2.6.0](https://gitlab.com/fidget-code/sketti/compare/2.5.0...2.6.0)

> 17 January 2025

- Add release filter [`#4`](https://gitlab.com/fidget-code/sketti/merge_requests/4)
- Updates for README media [`#3`](https://gitlab.com/fidget-code/sketti/merge_requests/3)

### CI Commits

- CI Version 2.6.0  [`a88017d`](https://gitlab.com/fidget-code/sketti/commit/a88017da8f28e8432504be8a5f9b7950044bf1de) *17 January 2025*
### [2.5.0](https://gitlab.com/fidget-code/sketti/compare/2.4.0...2.5.0)

> 13 January 2025

### Commits

- Improve output for loops @mediatech15 [`ef6ca91`](https://gitlab.com/fidget-code/sketti/commit/ef6ca91e4ccf38f40aaf144a0d2c76ec5678cce3) *13 January 2025*

### CI Commits

- CI Version 2.5.0  [`f124635`](https://gitlab.com/fidget-code/sketti/commit/f124635f339b28e850c65c18e23856e811a3c48d) *13 January 2025*
### [2.4.0](https://gitlab.com/fidget-code/sketti/compare/2.3.0...2.4.0)

> 13 January 2025

### CI Commits

- CI Version 2.4.0  [`55b6036`](https://gitlab.com/fidget-code/sketti/commit/55b6036e6018b60076d7140dd66d677035ebdccf) *13 January 2025*
- Update CI for issues with glab releases @mediatech15 [`fc7a161`](https://gitlab.com/fidget-code/sketti/commit/fc7a161cf99377fc17f9eca43e4346282fd7e1ff) *13 January 2025*
### [2.3.0](https://gitlab.com/fidget-code/sketti/compare/2.2.0...2.3.0)

> 13 January 2025

### CI Commits

- CI Version 2.3.0  [`12ce03a`](https://gitlab.com/fidget-code/sketti/commit/12ce03afd3377d637f4b9238b172e76c35337b34) *13 January 2025*
- Update CI for issues with glab releases @mediatech15 [`ebb335b`](https://gitlab.com/fidget-code/sketti/commit/ebb335b224eebe840ddd0010ce5c414ed0b7aa8b) *13 January 2025*
### [2.2.0](https://gitlab.com/fidget-code/sketti/compare/2.1.0...2.2.0)

> 13 January 2025

### CI Commits

- CI Version 2.2.0  [`d494cb6`](https://gitlab.com/fidget-code/sketti/commit/d494cb69c661ac0ee566e4949899f0549cb9d5d2) *13 January 2025*
### 2.1.0

> 13 January 2025

### Commits

- Update file extensions for proper rendering @mediatech15 [`d98ebe7`](https://gitlab.com/fidget-code/sketti/commit/d98ebe7e4c6f94b38884223a5feeff0bc6fd5e91) *13 January 2025*
- Update version for rust version to 2.0.0 @mediatech15 [`b4b4731`](https://gitlab.com/fidget-code/sketti/commit/b4b4731616099ff922c0551f6fcff63cebad2bde) *13 January 2025*
- Updates for CHANGELOG @mediatech15 [`5a2ebdc`](https://gitlab.com/fidget-code/sketti/commit/5a2ebdceb21a027b189bb60c6f8e27fb5ed7a9d2) *10 January 2025*
- Add gitattributes @mediatech15 [`7d44d66`](https://gitlab.com/fidget-code/sketti/commit/7d44d6606129b6753d1f49cbd02b6a445a2f0738) *10 January 2025*
- Update readme and minor bug @mediatech15 [`ce10915`](https://gitlab.com/fidget-code/sketti/commit/ce10915ec4262588d5aa10c86ebe3d083bdd5074) *10 January 2025*
- Add CHANGELOG @mediatech15 [`d5a3877`](https://gitlab.com/fidget-code/sketti/commit/d5a38771ed41fd4e96548ca4418dd8ca9af82579) *9 January 2025*
- Add LICENSE @mediatech15 [`95de94a`](https://gitlab.com/fidget-code/sketti/commit/95de94af4f625b8a2417a5bd8f0745dfa7a207a3) *9 January 2025*
- Merge rewrite-rust branch into main. @mediatech15 [`d97256d`](https://gitlab.com/fidget-code/sketti/commit/d97256d10b59f0e757090f392e0b50cb85b54f16) *8 January 2025*
- Push backup of old nodejs version @mediatech15 [`c4c8e41`](https://gitlab.com/fidget-code/sketti/commit/c4c8e4142a196af94eee98e354f8ecbdbddfab04) *9 December 2024*
- Initial commit @mediatech15 [`a14f85e`](https://gitlab.com/fidget-code/sketti/commit/a14f85e864f6e7805d271129d044a4b14e569577) *14 January 2024*

### CI Commits

- CI Version 2.1.0  [`cc09edc`](https://gitlab.com/fidget-code/sketti/commit/cc09edc739293e7c38afdc35cd690b9762efc46e) *13 January 2025*
- Fix CI. Update tagging for CHANGELOG @mediatech15 [`2ecb4a6`](https://gitlab.com/fidget-code/sketti/commit/2ecb4a62d12883125dc60d191a8c2842ed423b28) *10 January 2025*
- Fix CI. Update script execution bit. @mediatech15 [`47101bf`](https://gitlab.com/fidget-code/sketti/commit/47101bfdb45bf4fa6df971d3da18407995e32a7d) *10 January 2025*
- Update CI for consistency @mediatech15 [`c2ba4e9`](https://gitlab.com/fidget-code/sketti/commit/c2ba4e9c73556c4135403419d9862098f140a3f6) *10 January 2025*
- Fix CI for main branch. @mediatech15 [`ea5fc90`](https://gitlab.com/fidget-code/sketti/commit/ea5fc90edd4e232a32378f8d4bf79d8fa3c06096) *8 January 2025*
